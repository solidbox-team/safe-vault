Source: safe-vault
Section: text
Priority: optional
Maintainer: Jonas Smedegaard <dr@jones.dk>
Homepage: https://safenetwork.tech/faq/#what-is-a-vault
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/safe-team/safe-vault.git
Vcs-Browser: https://salsa.debian.org/safe-team/safe-vault
Rules-Requires-Root: no
Build-Depends:
 bash-completion,
 ca-certificates,
 cargo:native,
 debhelper-compat (= 13),
 dh-cargo,
 libstd-rust-dev,
# librust-base64-0.10+default-dev (>= 0.10.1-~~),
 librust-base64-0.12+default-dev,
# librust-bincode-1.1.4+default-dev,
 librust-bincode-1.2.1+default-dev,
 librust-bytes-0.4+default-dev (>= 0.4.12-~~),
 librust-bytes-0.4+serde-dev (>= 0.4.12-~~),
# librust-bytes-0.5+default-dev (>= 0.5.4-~~),
# librust-bytes-0.5+serde-dev (>= 0.5.4-~~),
 librust-crossbeam-channel-0.4+default-dev (>= 0.4.2-~~),
 librust-ctrlc-3.1+default-dev (>= 3.1.3-~~),
 librust-directories-2.0+default-dev (>= 2.0.1-~~),
# librust-fake-clock-0.3+default-dev,
# librust-flexy-logger-0.14.8+default-dev (>= 0.14.8-~~),
# librust-hex-0.3+default-dev (>= 0.3.2-~~),
 librust-hex-0.4+default-dev,
# librust-hex-fmt-0.3+default-dev,
# librust-lazy-static-1.3+default-dev,
 librust-lazy-static-1.4+default-dev,
# librust-log-0.4+default-dev (>= 0.4.7-~~),
 librust-log-0.4+default-dev (>= 0.4.8-~~),
# librust-pickledb-0.4+default-dev,
# librust-quick-error-1.2+default-dev (>= 1.2.2-~~),
 librust-quick-error-1.2+default-dev (>= 1.2.3-~~),
# librust-rand-0.6+default-dev (>= 0.6.5-~~),
 librust-rand-0.7+default-dev (>= 0.7.2-~~),
# librust-rand-chacha-0.1+default-dev (>= 0.1.1-~~),
 librust-rand-chacha-0.2+default-dev (>= 0.2.2-~~),
 librust-rand-core-0.5+default-dev (>= 0.5.1-~~),
# librust-routing-dev,
# librust-safe-nd-0.9+default-dev,
# librust-self-update-0.16+default-dev,
 librust-serde-1.0+default-dev (>= 1.0.97-~~),
# librust-serde-1.0+derive-dev (>= 1.0.97-~~),
 librust-serde-json-1.0+default-dev (>= 1.0.40-~~),
# librust-structopt-0.2+default-dev (>= 0.2.18-~~),
 librust-structopt-0.3+default-dev,
# librust-tiny-keccak-1.5+default-dev,
 librust-unwrap-1.2+default-dev (>= 1.2.1-~~),
 rustc:native,

Package: safe-vault
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 bash-completion,
Description: implementation of Vault node for the SAFE Network
 The "Vault" is a service for the SAFE Network
 handling encrypted storage.
 .
 The SAFE (Secure Access For Everyone) Network
 is a software framework for decentralized, peer-to-peer networking
 with autonomous resource accountability:
 you earn credits by contributing disk space, network bandwidth
 and CPU time to the SAFE Network,
 which you can spend on storing data securily and distributed
 on the network.
